import React from 'react';
import ReactDOM from 'react-dom';
import {Router} from "react-router-dom";
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import thunk from "redux-thunk";
import {Provider} from "react-redux";
import App from './App';
import postsReducer from "./store/reducers/postsReducer";
import {createTheme, MuiThemeProvider} from "@material-ui/core";
import usersReducer from "./store/reducers/usersReducer";
import history from "./history";
import commentsReducer from "./store/reducers/commentsReducer";

const saveToLocalStorage = state => {
	try {
		const serialized = JSON.stringify(state);
		localStorage.setItem('postsState', serialized);
	} catch (e) {
		console.log('Could not save state');
	}
};

const loadFromLocalStorage = () => {
	try {
		const serializedStare = localStorage.getItem('postsState');

		if (serializedStare === null) {
			return undefined;
		}

		return JSON.parse(serializedStare);
	} catch (e){
		return undefined;
	}
};

const rootReducer = combineReducers({
	posts: postsReducer,
	users: usersReducer,
	comments: commentsReducer,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const  persistedState = loadFromLocalStorage();

const store = createStore(
	rootReducer,
	persistedState,
	composeEnhancers(applyMiddleware(thunk)),
);

store.subscribe(() => {
	saveToLocalStorage({
		users: store.getState().users,
	});
});

const theme = createTheme({
	props: {
		MuiTextField: {
			variant: "outlined",
			fullWidth: true,
		},
	},
});

const app = (
	<Provider store={store}>
		<Router history={history}>
			<MuiThemeProvider theme={theme}>
				<App/>
			</MuiThemeProvider>
		</Router>
	</Provider>
);

ReactDOM.render(app, document.getElementById('root'));
