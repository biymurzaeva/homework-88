import axiosApi from "../../axiosApi";

export const CREATE_COMMENT_REQUEST = 'CREATE_COMMENT_REQUEST';
export const CREATE_COMMENT_SUCCESS = 'CREATE_COMMENT_SUCCESS';
export const CREATE_COMMENT_FAILURE = 'CREATE_COMMENT_FAILURE';

export const FETCH_COMMENTS_REQUEST = 'FETCH_COMMENTS_REQUEST';
export const FETCH_COMMENTS_SUCCESS = 'FETCH_COMMENTS_SUCCESS';
export const FETCH_COMMENTS_FAILURE = 'FETCH_COMMENTS_FAILURE';

export const createCommentRequest = () => ({type: CREATE_COMMENT_REQUEST});
export const createCommentSuccess = () => ({type: CREATE_COMMENT_SUCCESS});
export const createCommentFailure = error => ({type: CREATE_COMMENT_FAILURE, payload: error});

export const fetchCommentsRequest = () => ({type: CREATE_COMMENT_REQUEST});
export const fetchCommentsSuccess = comments => ({type: CREATE_COMMENT_SUCCESS, payload: comments});
export const fetchCommentsFailure = () => ({type: CREATE_COMMENT_SUCCESS});

export const createComment = commentData => {
	return async (dispatch, getState) => {
		const headers = {
			'Authorization': getState().users.user && getState().users.user.token
		};

		try {
			dispatch(createCommentRequest());
			await axiosApi.post('/comments' , commentData, {headers});
			dispatch(createCommentSuccess());
		} catch (error) {
			dispatch(createCommentFailure());
		}
	}
};

export const fetchComments = id => {
	return async dispatch => {
		try {
			dispatch(fetchCommentsRequest());
			const response = await axiosApi.get('comments?post_id=' + id);
			dispatch(fetchCommentsSuccess(response.data));
		} catch (error) {
			dispatch(fetchCommentsFailure());
		}
	};
};