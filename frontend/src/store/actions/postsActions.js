import axiosApi from "../../axiosApi";

export const FETCH_POSTS_REQUEST = 'FETCH_POSTS_REQUEST';
export const FETCH_POSTS_SUCCESS = 'FETCH_POSTS_SUCCESS';
export const FETCH_POSTS_FAILURE = 'FETCH_POSTS_FAILURE';

export const FETCH_POST_REQUEST = 'FETCH_POST_REQUEST';
export const FETCH_POST_SUCCESS = 'FETCH_POST_SUCCESS';
export const FETCH_POST_FAILURE = 'FETCH_POST_FAILURE';

export const CREATE_POST_REQUEST = 'CREATE_POST_REQUEST';
export const CREATE_POST_SUCCESS = 'CREATE_POST_SUCCESS';
export const CREATE_POST_FAILURE = 'CREATE_POST_FAILURE';

export const fetchPostsRequest = () => ({type: FETCH_POSTS_REQUEST});
export const fetchPostsSuccess = posts => ({type: FETCH_POSTS_SUCCESS, payload: posts});
export const fetchPostsFailure = error => ({type: FETCH_POSTS_FAILURE, payload: error});

export const fetchPostRequest = () => ({type: FETCH_POST_REQUEST});
export const fetchPostSuccess = post => ({type: FETCH_POST_SUCCESS, payload: post});
export const fetchPostFailure = error => ({type: FETCH_POST_FAILURE, payload: error});

export const createPostRequest = () => ({type: CREATE_POST_REQUEST});
export const createPostSuccess = () => ({type: CREATE_POST_SUCCESS});
export const createPostFailure = error => ({type: CREATE_POST_FAILURE, payload: error});

export const fetchPosts = () => {
	return async dispatch => {
		try {
			dispatch(fetchPostsRequest());
			const response = await axiosApi.get('/posts');
			dispatch(fetchPostsSuccess(response.data));
		} catch (error) {
			dispatch(fetchPostsFailure(error));
		}
	};
};

export const fetchPost = id => {
	return async (dispatch) => {
		try {
			dispatch(fetchPostRequest());
			const response = await axiosApi.get(`/posts/${id}`);
			dispatch(fetchPostSuccess(response.data));
		} catch (error) {
			dispatch(fetchPostFailure(error));
		}
	};
};

export const createPost = postData => {
	return async (dispatch, getState) => {
		const headers = {
			'Authorization': getState().users.user && getState().users.user.token
		};

		try {
			dispatch(createPostRequest());
			await axiosApi.post('/posts', postData, {headers});
			dispatch(createPostSuccess());
		} catch (error) {
			dispatch(createPostFailure(error));
		}
	};
};