import {
	CREATE_POST_FAILURE,
	CREATE_POST_REQUEST,
	CREATE_POST_SUCCESS,
	FETCH_POST_FAILURE,
	FETCH_POST_REQUEST,
	FETCH_POST_SUCCESS,
	FETCH_POSTS_FAILURE,
	FETCH_POSTS_REQUEST,
	FETCH_POSTS_SUCCESS
} from "../actions/postsActions";

const initialState = {
	fetchLoading: false,
	createPostLoading: false,
	postLoading: false,
	posts: [],
	error: null,
	post: {},
	postError: null,
};

const postsReducer = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_POSTS_REQUEST:
			return {...state, fetchLoading: true};
		case FETCH_POSTS_SUCCESS:
			return {...state, fetchLoading: false, posts: action.payload};
		case FETCH_POSTS_FAILURE:
			return {...state, fetchLoading: false, error: action.payload};
		case FETCH_POST_REQUEST:
			return {...state, postLoading: true};
		case FETCH_POST_SUCCESS:
			return {...state, postLoading: false, post: action.payload};
		case FETCH_POST_FAILURE:
			return {...state, postLoading: false, postError: action.payload};
		case CREATE_POST_REQUEST:
			return {...state, createPostLoading: true};
		case CREATE_POST_SUCCESS:
			return {...state, createPostLoading: false, error: null};
		case CREATE_POST_FAILURE:
			return {...state, createPostLoading: false, error: action.payload}
		default:
			return state;
	}
};

export default postsReducer;