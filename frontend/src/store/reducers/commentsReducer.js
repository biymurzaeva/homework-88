import {
	CREATE_COMMENT_FAILURE,
	CREATE_COMMENT_REQUEST,
	CREATE_COMMENT_SUCCESS, FETCH_COMMENTS_FAILURE,
	FETCH_COMMENTS_REQUEST, FETCH_COMMENTS_SUCCESS
} from "../actions/commentsActions";

const initialState = {
	commentLoading: false,
	fetchLoading: false,
	commentError: null,
	comments: null,
};

const commentsReducer = (state = initialState, action) => {
	switch (action.type) {
		case CREATE_COMMENT_REQUEST:
			return {...state, commentLoading: true};
		case CREATE_COMMENT_SUCCESS:
			return {...state, commentError: null, commentLoading: false};
		case CREATE_COMMENT_FAILURE:
			return {...state, commentLoading: false, commentError: action.payload};
		case FETCH_COMMENTS_REQUEST:
			return {...state, fetchLoading: true};
		case FETCH_COMMENTS_SUCCESS:
			return {...state, fetchLoading: false, comments: action.payload};
		case FETCH_COMMENTS_FAILURE:
			return {...state, fetchLoading: false, comments: null};
		default:
			return state;
	}
};

export default commentsReducer;