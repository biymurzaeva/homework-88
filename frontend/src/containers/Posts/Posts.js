import React, {useEffect} from 'react';
import {CircularProgress, Grid} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {fetchPosts} from "../../store/actions/postsActions";
import Post from "../../components/Post/Post";

const Posts = () => {
	const dispatch = useDispatch();
	const fetchLoading = useSelector(state => state.posts.fetchLoading);
	const posts = useSelector(state => state.posts.posts);

	useEffect(() => {
		dispatch(fetchPosts());
	}, [dispatch]);

	return (
		<Grid container direction="column" spacing={2}>
			<Grid item>
				<Grid item container direction="column" xs>
					{fetchLoading ? (
						<Grid container justifyContent="center" alignItems="center">
							<Grid item>
								<CircularProgress/>
							</Grid>
						</Grid>
					) : posts.map(post => (
						<Post
							key={post._id}
							id={post._id}
							title={post.title}
							image={post.image}
							postDate={post.date}
							author={post.author}
						/>
					))}
				</Grid>
			</Grid>
		</Grid>
	);
};

export default Posts;