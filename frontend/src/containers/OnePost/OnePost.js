import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchPost} from "../../store/actions/postsActions";
import {Grid, makeStyles, Typography} from "@material-ui/core";
import {apiURL} from "../../config";
import CommentForm from "../../components/CommentForm/CommentForm";
import {fetchComments} from "../../store/actions/commentsActions";

const useStyles = makeStyles({
	image: {
		maxWidth: "250px",
		maxHeight: "250px",
	},
});

const OnePost = ({match}) => {
	const classes = useStyles();
	const dispatch = useDispatch();
	const post = useSelector(state => state.posts.post);
	const user = useSelector(state => state.users.user);

	useEffect(() => {
		dispatch(fetchPost(match.params.id));
		dispatch(fetchComments(match.params.id));
	}, [dispatch, match.params.id]);

	const date = new Date(post.date);
	let year = date.getFullYear();
	let month = date.getMonth()+1;
	let dt = date.getDate();
	let hour = date.getHours();
	let minutes = date.getMinutes();

	if (dt < 10) {
		dt = '0' + dt;
	}

	if (month < 10) {
		month = '0' + month;
	}

	return post && (
		<Grid item container direction="row" spacing={1}>
			<Grid item>
				{post.image ? <img src={apiURL + '/uploads/' + post.image} alt={post.title} className={classes.image}/> : <></>}
				<Typography component="h1" variant="h6">{post['title']}</Typography>
				<Typography variant="subtitle1">
					{dt + '.' + month + '.' + year + ' ' + hour + ':'  + minutes} by <strong>{post.author}</strong>
				</Typography>
				<Typography variant="subtitle1">{post.description}</Typography>
				{user ? <CommentForm/> : <></>}
			</Grid>
		</Grid>
	);
};

export default OnePost;