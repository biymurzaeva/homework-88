import React from 'react';
import {useDispatch} from "react-redux";
import {createPost} from "../../store/actions/postsActions";
import PostForm from "../../components/PostForm/PostForm";

const AddPost = ({history}) => {
	const dispatch = useDispatch();

	const onSubmit = async postData => {
		await dispatch(createPost(postData));
		history.replace('/');
	};

	return (
		<>
			<PostForm onSubmit={onSubmit}/>
		</>
	);
};

export default AddPost;