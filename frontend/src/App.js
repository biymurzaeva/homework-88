import Layout from "./components/UI/Layout/Layout";
import {Route, Switch} from "react-router-dom";
import Posts from "./containers/Posts/Posts";
import AddPost from "./containers/AddPost/AddPost";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import OnePost from "./containers/OnePost/OnePost";

const App = () => (
	<Layout>
		<Switch>
			<Route path='/' exact component={Posts}/>
			<Route path='/post/add' component={AddPost}/>
			<Route path='/register' component={Register}/>
			<Route path='/login' component={Login}/>
			<Route path='/posts/:id' component={OnePost}/>
		</Switch>
	</Layout>
);

export default App;
