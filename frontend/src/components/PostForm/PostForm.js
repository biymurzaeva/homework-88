import React, {useState} from 'react';
import {Button, Grid, makeStyles, TextField} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
	root: {
		marginTop: theme.spacing(2)
	},
}));

const PostForm = ({onSubmit}) => {
	const classes = useStyles();

	const [state, setState] = useState({
		title: '',
		description: '',
		image: null,
	});

	const inputChangeHandler = e => {
		const {name, value} = e.target;
		setState(prevState => ({...prevState, [name]: value}));
	};

	const fileChangeHandler = e => {
		const name = e.target.name;
		const file = e.target.files[0];
		setState(prevState => ({...prevState, [name]: file}));
	};

	const submitFormHandler = e => {
		e.preventDefault();

		const formData = new FormData();

		Object.keys(state).forEach(key => {
			formData.append(key, state[key]);
		});

		onSubmit(formData);
	};

	return (
		<Grid
			container
			direction="column"
			spacing={2}
			component="form"
			className={classes.root}
			autoComplete="off"
			onSubmit={submitFormHandler}
		>
			<Grid item xs>
				<TextField
					fullWidth
					required
					variant="outlined"
					label="Title"
					name="title"
					value={state.title}
					onChange={inputChangeHandler}
				/>
			</Grid>
			<Grid item xs>
				<TextField
					fullWidth
					multiline
					rows={3}
					variant="outlined"
					label="Description"
					name="description"
					value={state.description}
					onChange={inputChangeHandler}
				/>
			</Grid>
			<Grid item xs>
				<TextField
					type="file"
					name="image"
					onChange={fileChangeHandler}
				/>
			</Grid>

			<Grid item xs>
				<Button
					type="submit"
					color="primary"
					variant="contained"
					disabled={!state.description && !state.image}
				>
					Create post
				</Button>
			</Grid>
		</Grid>
	);
};

export default PostForm;