import React from 'react';
import {Grid, makeStyles, Typography} from "@material-ui/core";
import ForumIcon from '@material-ui/icons/Forum';
import {apiURL} from "../../config";
import LinesEllipsis from "react-lines-ellipsis";
import {Link} from "react-router-dom";

const useStyles = makeStyles({
	postBlock: {
		border: "1px solid #ccc",
		borderRadius: "5px",
		padding: "15px",

	},
	postImage: {
		maxWidth: "100px",
		maxHeight: "100px",
	}
});

const Post = ({title, image, postDate, author, id}) => {
	const classes = useStyles();

	let forumImage = null;

	if (image) {
		forumImage = apiURL + '/uploads/' + image;
	}

	const date = new Date(postDate);
	let year = date.getFullYear();
	let month = date.getMonth()+1;
	let dt = date.getDate();
	let hour = date.getHours();
	let minutes = date.getMinutes();

	if (dt < 10) {
		dt = '0' + dt;
	}
	if (month < 10) {
		month = '0' + month;
	}

	return (
		<Grid item container direction="row" spacing={1} className={classes.postBlock}>
				<Grid item>
					{image ? <img src={forumImage} alt={title} className={classes.postImage}/> : <ForumIcon/>}
				</Grid>
				<Grid item>
					<Typography variant="subtitle1">
						{dt + '.' + month + '.' + year + ' ' + hour + ':'  + minutes} by <strong>{author}</strong>
					</Typography>
					<Link to={`/posts/${id}`}>
						<LinesEllipsis
							text={title}
							maxLine={1}
							ellipsis={'...'}
							trimRight
							basedOn='letters'
						/>
					</Link>
				</Grid>
			</Grid>
	);
};

export default Post;