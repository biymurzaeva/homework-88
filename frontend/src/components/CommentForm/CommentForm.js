import React, {useState} from 'react';
import {Button, Grid, makeStyles, TextareaAutosize} from "@material-ui/core";
import {useDispatch} from "react-redux";
import {createComment} from "../../store/actions/commentsActions";

const useStyles = makeStyles(theme => ({
	form: {
		marginTop: theme.spacing(5),
	},
	submit: {
		margin: theme.spacing(3, 0, 2),
	},
	textarea: {
		width: '100%',
		resize: 'none',
		padding: '10px',
		outline: "none",
	},
}));

const CommentForm = () => {
	const classes = useStyles();
	const dispatch = useDispatch();

	const [comment, setComment] = useState({
		comment: '',
	});

	const inputChange  = e => {
		const {name, value} = e.target;
		setComment(prevState => ({...prevState, [name]: value}));
	};

	const onFormSubmit  = e => {
		e.preventDefault();
		dispatch(createComment({...comment}));
	};

	return (
		<Grid
			item
			component="form"
			onSubmit={onFormSubmit}
			className={classes.form}
		>
			<TextareaAutosize
				required
				minRows={10}
				placeholder="Comment"
				name="comment"
				value={comment.comment}
				onChange={inputChange}
				className={classes.textarea}
			/>
			<Button
				type="submit"
				fullWidth
				variant="contained"
				color="primary"
				className={classes.submit}
			>
				Send
			</Button>
		</Grid>
	);
};

export default CommentForm;