import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchComments} from "../../store/actions/commentsActions";

const Comments = ({id}) => {
	const dispatch = useDispatch();
	const comments = useSelector(state => state.comments.comments);

	useEffect(() => {
		dispatch(fetchComments(id))
	}, [dispatch]);

	return comments && (
		<>
			{comments.map(comment => (
				<>
					<div>{comment.user_id}</div>
					<div>{comment.comment}</div>
				</>
			))}
		</>
	);
};

export default Comments;