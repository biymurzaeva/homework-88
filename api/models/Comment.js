const mongoose = require('mongoose');

const CommentSchema = new mongoose.Schema({
	user_id: String,
	post_id: String,
	comment: {
		type: String,
		required: true,
	},
});

const Comment = mongoose.model('Comment', CommentSchema);
module.exports = Comment;