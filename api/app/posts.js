const express = require('express');
const Post = require('../models/Post');
const path = require("path");
const multer = require("multer");
const {nanoid} = require("nanoid");
const config = require('../config');
const auth = require("../middleware/auth");

const router = express.Router();

const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, config.uploadPath);
	},
	filename: (req, file, cb) => {
		cb(null, nanoid() + path.extname(file.originalname));
	},
});

const upload = multer({storage});

router.get('/', async (req, res) => {
	try {
		const posts = await Post.find().sort("-date");
		res.send(posts);
	} catch (error) {
		res.sendStatus(500);
	}
});

router.get('/:id', async (req, res) => {
	try {
		const post = await Post.findById(req.params.id);

		if (post) {
			res.send(post);
		} else {
			res.status(404).send({error: 'Post not found'});
		}

	} catch (error) {
		res.sendStatus(500);
	}
});

router.post('/', auth, upload.single('image'), async (req, res) => {
	if (!req.body.title) {
		return res.status(400).send({error: 'Title is required'});
	}

	if (!req.body.description && req.body.description) {
		return res.status(400).send({error: 'Please fill in one of the fields'});
	}

	const postData = {
		title: req.body.title,
		date: new Date().toISOString(),
		author: req.user.username,
	};

	if (req.body.description) {
		postData.description = req.body.description;
	}

	if (req.file) {
		postData.image = req.file.filename;
	}

	const post = new Post(postData);

	try {
		await post.save();
		res.send(post);
	} catch (error) {
		res.status(400).send({error: 'Data not valid'});
	}
});

module.exports = router;