const express = require('express');
const Comment = require('../models/Comment');
const auth = require("../middleware/auth");

const router = express.Router();

router.get('/', async (req, res) => {
	try {
		const query = {};

		if (req.query.post_id) {
			query.post_id = req.query.post_id;
		}

		const comments = await Comment.find(query);
		res.send(comments);
	} catch (error) {
		res.sendStatus(500);
	}
});

router.post('/', auth, async (req, res) => {
	if (!req.body.comment) {
		return res.status(400).send({error: 'Comment is required'});
	}

	const commentData = {
		comment: req.body.comment,
		user_id: req.user._id,
		post_id: req.query.post_id,
	};

	const comment = new Comment(commentData);

	try {
		await comment.save();
		res.send(comment);
	} catch (error) {
		res.status(400).send({error: 'Data not valid'});
	}
});

module.exports = router;